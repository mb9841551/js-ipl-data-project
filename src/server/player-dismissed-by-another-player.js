// Find the highest number of times one player has been dismissed by another player

const csv = require("csv-parser");         //Imported the csv-parser module which helps in parsing csv files.
const fs = require("fs");                  //Imported the fs module which helps in reading and writing into csv files.

const deliveriesData = [];

fs.createReadStream("../data/deliveries.csv")      //fs module reads from the path, then pipes it to csv writable stream and perform event handling
.pipe(csv())
.on("data" , (data) => deliveriesData.push(data))
.on("error" , (error) => {
    if(error){
        console.log(error);
    }
})
.on("end" , () => {
    // console.log(deliveriesData);
    playerDismissedByAnotherPlayer(deliveriesData);  //call the function
})

function playerDismissedByAnotherPlayer(deliveriesData){
    let ansObj = {};  //empty object
    for(let delivery of deliveriesData ){
        if(delivery.player_dismissed !== ""){     //if there is player which is dismissed
            if(ansObj.hasOwnProperty(delivery.bowler) === false){   //if ansObj has delivery.bowler property or not
                ansObj[delivery.bowler] = {};   //emoty object
                ansObj[delivery.bowler][delivery.player_dismissed]=1;   //assign plyer dismissed property of bowler to 1 
            }
            else{
                if(ansObj[delivery.bowler].hasOwnProperty(delivery.player_dismissed) === false){    //if current player dismissed is not property of bowler
                    ansObj[delivery.bowler][delivery.player_dismissed] = 1;    //assign it to 1
                }
                else{
                    ansObj[delivery.bowler][delivery.player_dismissed]++;    //add to the previous value
                }
            }
        }
    }
    // console.log(ansObj);
    let max = Number.MIN_VALUE;
    let finalObj ={};   //empty object
    let bowlers = Object.keys(ansObj);    //to get bowlers of ansObj
    for(let key of bowlers){             
        for(let bat in ansObj[key]){     //to iterate through ansObj[key] which are batsmen
            let dismiss = ansObj[key][bat];    
            // console.log();
            if(ansObj[key][bat] > max){
                max = ansObj[key][bat];  //get the maximum time bowler dismissed batsman
                finalObj = { bowler : key ,batsman : bat ,dismmissal : ansObj[key][bat]};    //to assign the object with values
            }
        }
    }
    fs.writeFile("../public/output/playerDismissedByAnotherPLayer.json",JSON.stringify(finalObj),(error) => {   //fs module writes or dump it as JSON file into specified path and error is used as callback function
        if(error){
            console.log(error);
        }
    })
}
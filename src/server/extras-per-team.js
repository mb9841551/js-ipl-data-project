// Extra runs conceded per team in the year 2016

const csv = require("csv-parser");            //Imported the csv-parser module which helps in parsing csv files.
const fs = require("fs");                     //Imported the fs module which helps in reading and writing into csv files.

const matchData = [];
const deliveriesData = [];


fs.createReadStream("../data/matches.csv")         //fs module reads from the path, then pipes it to csv writable stream and perform event handling
.pipe(csv())
.on("data" , (data) => {
    matchData.push(data);                          //push the incoming data in form of rows to matchesData Array
})
.on("error" , (error) => {
    if(error){
        console.log("error detected");
    }
})
.on("end" , () => {
    const id = [];              //created empty array
    for(let match of matchData){
        if(match.season === "2016"){
            id.push(match.id);    //pushed all the ids of 2016 into this array
        }
    }
    fs.createReadStream("../data/deliveries.csv")
    .pipe(csv())
    .on("data" , (data) => deliveriesData.push(data))                      //push the incoming data in form of rows to deliveriesData Array
    .on("error" ,(error) => console.log("Error detected"))
    .on("end" , (end) => {
        const ansObj ={};     //empty object
        for(let idno of id){                 //iterating over each id
            for(let deliveryDetail of deliveriesData){       
                if(deliveryDetail.match_id === idno){   //if deliveryDetail matchid is equal with idno of id array
                    if(ansObj.hasOwnProperty(deliveryDetail.bowling_team) === true){  //check if ansObj has bowlimgTeam property
                        ansObj[deliveryDetail.bowling_team] += Number(deliveryDetail.extra_runs);  //if yes, then assign into it the and addd the previous extra runs 
                    }
                    else{
                        ansObj[deliveryDetail.bowling_team] = Number(deliveryDetail.extra_runs);   //if no , then assign extra runs to bowling team
                    }
                }
            }
        }
        // console.log(ansObj);
        fs.writeFile("../public/output/extrasPerTeam.json",           //fs module writes or dump it as JSON file into specified path and error is used as callback function
        JSON.stringify(ansObj),
        (error)=>{
            if(error){
                console.log(error);
            }
        });
    });
});
// Number of matches won per team per year in IPL.

const csv = require("csv-parser");   //Imported the csv-parser module which helps in parsing csv files.
const fs = require("fs");            //Imported the fs module which helps in reading and writing into csv files.

let matchData = [];

fs.createReadStream("../data/matches.csv")           //fs module reads from the path, then pipes it to csv writable stream and perform event handling
.pipe(csv())
.on("data" , (data) => {
    matchData.push(data);                            //push the incoming data in form of rows to matchesData Array
})
.on("error" , (error) => {
    if(error){
        console.log("Error occured");
    }
})
.on("end" ,() => {
    matchesWonPerTeamPerYear(matchData);           //called the function when fs module get the event "end"
});

function matchesWonPerTeamPerYear(matchData){
    let ansObj = {}; //empty object
    
    for(let matches of matchData){
        let season = matches.season;   //
        let winner = matches.winner;
        if(ansObj.hasOwnProperty(season) === false){    //to check if ansObj has season property or not
            ansObj[season] = {};            //if not assign season as empty object which is property of ansObj.
        }
        if(ansObj[season].hasOwnProperty(winner) === false){ //to check if season has winner property or not
            ansObj[season][winner] = 1;    //if not assign team value of winner to be 1.
        }
        else{
            ansObj[season][winner]++;      //if the team is already won , add it to current tally.
        }
    }
    fs.writeFile("../public/output/matchesWonPerTeamPerYear.json",  //fs module writes or dump it as JSON file into specified path and error is used as callback function
    JSON.stringify(ansObj),
    (error) => {
        if(error){
            console.log("Error Occurred");
        }
    });
}
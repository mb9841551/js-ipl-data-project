// Number of matches played per year for all the years in IPL.

const csv = require("csv-parser");            //Imported the csv-parser module which helps in parsing csv files.
const fs = require("fs");                     //Imported the fs module which helps in reading and writing into csv files.

let matchData = [];                         

fs.createReadStream("../data/matches.csv")     //fs module reads from the path, then pipes it to csv writable stream and perform event handling
.pipe(csv())
.on("data" , (data) => matchData.push(data))    //push the incoming data in form of rows to matchData Array
.on("error" , (error) => console.log(error))
.on("end" , () => {
    let ansObj = {}; //created empty object 

    for(let years of matchData){
        if(ansObj.hasOwnProperty(years.season)){ //if season is present in ansObj 
            ansObj[years.season]++;  //if yes, then increment it by one from thr previous value
        }
        else{
            ansObj[years.season] = 1; //if no , then assign it 1.
        }
    }
    fs.writeFile("../public/output/matchesPerYear.json",     //fs module writes or dump it as JSON file into specified path and error is used as callback function
    JSON.stringify(ansObj),
    (error) => {
        if(error){
            console.log(error);
        }
    }
    );
});
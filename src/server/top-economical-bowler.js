// Top 10 economical bowlers in the year 2015

const csv = require("csv-parser");           //Imported the csv-parser module which helps in parsing csv files.
const fs = require("fs");                    //Imported the fs module which helps in reading and writing into csv files.

const matchData = [];

fs.createReadStream("../data/matches.csv")           //fs module reads from the path, then pipes it to csv writable stream and perform event handling
.pipe(csv())
.on("data" , (data) => matchData.push(data))                        //push the incoming data in form of rows to matchData Array
.on("error" , (error) => console.log(error))
.on("end" , () => {
    const deliveriesData = [];
    fs.createReadStream("../data/deliveries.csv")                    //push the incoming data in form of rows to deliveriesData Array
    .pipe(csv())
    .on("data" , (data) => deliveriesData.push(data))
    .on("error" ,(error) => console.log(error))
    .on("end" , () => {
        topEconomicalBowler(matchData , deliveriesData );
    })
})

function topEconomicalBowler(matchData , deliveriesData ){
    const id = [];
    for(let match of matchData){
        if(match.season === "2015"){
            id.push(match.id);         //pushed all the ids of 2015 into this array
        }
    }                                    //created three empty object which will have
    const totalRunsConceded = {};       // bowler name and total runs concede
    const totalBalls = {};              // bowler name and total balls 
    const economy = {};                 // bowler name and economy(which will be calculated by help of previous two objects).
    for(let idno of id){
        for(let deliveryDetail of deliveriesData){
            let bowler = deliveryDetail.bowler;
            if(idno === deliveryDetail.match_id){
                if(totalRunsConceded.hasOwnProperty(bowler) === true || totalBalls.hasOwnProperty(bowler) === true){   //if totalrunsconceded and total balls object contain bowler
                    totalRunsConceded[bowler] += parseFloat(deliveryDetail.total_runs); //assign total runs and add it to the previous value
                    totalBalls[bowler] += 1; //add it by 1 .
                }
                else{
                    totalRunsConceded[bowler] = parseFloat(deliveryDetail.total_runs);    // assign total runs to the totalconceded object in bowlers property
                    totalBalls[bowler] = 1;     // assign value to be 1.
                }
            }
        }
        const bowlerNames = Object.keys(totalBalls);  //get bowler names using object.key method
        for(let key of bowlerNames){
            economy[key] = parseFloat((totalRunsConceded[key]/totalBalls[key])*6);    //getting economy object and assigning its key values to economy value by calculation
        }
    }
    const finalArray = Object.entries(economy).sort(([,economy1],[,economy2])=> {     //sort economy object , first by converting it to array then apply sort function based on its economy
        if(economy1 !== economy2){
            return economy1-economy2;
        }
    }).slice(0,10);    //to get first 10 values

    fs.writeFile("../public/output/topEConomicalBowler.json",        //fs module writes or dump it as JSON file into specified path and error is used as callback function
    JSON.stringify(finalArray),
    (error) => {
        if(error){
            console.log(error);
        }
    });
}
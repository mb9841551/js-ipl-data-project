// Find a player who has won the highest number of Player of the Match awards for each season

const csv = require("csv-parser");         //Imported the csv-parser module which helps in parsing csv files.
const fs = require("fs");                  //Imported the fs module which helps in reading and writing into csv files.

const matchData = [];

fs.createReadStream("../data/matches.csv")            //fs module reads from the path, then pipes it to csv writable stream and perform event handling
.pipe(csv())
.on("data" , (data) => matchData.push(data))          //push the incoming data in form of rows to matchesData Array
.on("error" , (error) => console.log(error))
.on("end" , () => {
    highestPlayerOfMatch(matchData);            //call the function
})

function highestPlayerOfMatch(matchData){
    let ansObj = {};   //empty object
    for(let individualMatchData of matchData){
        let season = individualMatchData.season;
        let player_of_match = individualMatchData.player_of_match;
        if(ansObj.hasOwnProperty(season) === false){    //if ansObj has season property or not
            ansObj[season] = {};  //if not, then assign season as an empty object (object->object-(which is empty)
        }  
        if(ansObj[season].hasOwnProperty(player_of_match) === false){   //if season object (is inside ansObj) has player of the match property or not
            ansObj[season][player_of_match] = 1;        // if not then assign it to 1
        }
        else{
            ansObj[season][player_of_match]++;   //if yes, then add 1 to the previous value
        }
    }
    // console.log(ansObj);
    getHighestPlayerOfMatch(ansObj);   //call function to get player who got highest player of match
}

function getHighestPlayerOfMatch(data){
    const highestScorers = {};   //empty object

    for(let yearWithDetails in data){
        const yearData = data[yearWithDetails];     //to get data of a particular year(player of the match in a season)
        const highestPlayer = findHighestPlayerOfMatch(yearData);  //call the function to get highest player of the match in that season
        highestScorers[yearWithDetails]= highestPlayer;  //to assign highest player to the their particular season 
    }
    fs.writeFile("../public/output/highestPlayerOfMtach.json",JSON.stringify(highestScorers),(error) => {  //fs module writes or dump it as JSON file into specified path and error is used as callback function
        if (error) {
            console.error('Error writing to file:', error);
          }
    });
}

function findHighestPlayerOfMatch(yearData){
    let highestScore = 0;
    let highestPlayer = [];    //empty array

    for(let player in yearData){      //to get the highest player who got player of the match award 
        let score = yearData[player];
        if(score > highestScore){
            highestScore = score;
            highestPlayer[0] = player;    // assign the player with highest at 1st index.
        }
    }
    for(let player in yearData){
        let score = yearData[player];
        if(score == highestScore && highestPlayer[0] !== player){  //if any other player got same no of award then push it to that array.
            highestPlayer.push(player);
        }
    }
    return highestPlayer;
}

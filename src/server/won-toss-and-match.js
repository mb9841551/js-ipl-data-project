// Find the number of times each team won the toss and also won the match

const csv = require("csv-parser");           //Imported the csv-parser module which helps in parsing csv files.
const fs = require("fs");                    //Imported the fs module which helps in reading and writing into csv files.

const ansObj = {};
const matchData = [];

fs.createReadStream("../data/matches.csv")              //fs module reads from the path, then pipes it to csv writable stream and perform event handling
.pipe(csv())
.on("data" , (data) => matchData.push(data))           
.on("error" , (error) => console.log(error))
.on("end" , () => {
    wonTossAndMatch(matchData);              //call the function 
})

function wonTossAndMatch(matchData){
    const ansObj ={};    //empty object
    for(let match of matchData){
        tossWinner = match.toss_winner;    
        winner = match.winner;
        if(tossWinner === winner){    //if toss winner and winner are same 
            if(ansObj.hasOwnProperty(winner) === false){   //to check if ansObj has winner property or not
                ansObj[winner] = 1;  //if not , then assign it to 1
            }
            else{
                ansObj[winner]++;    //if yes, then add 1 to previous value
            }
        }
    }
    fs.writeFile("../public/output/wonTossAndMatch.json",JSON.stringify(ansObj),(error) => {     //fs module writes or dump it as JSON file into specified path and error is used as callback function
        if (error) {
            console.error('Error writing to file:', error);
          }
        // console.log(error);
    });
}
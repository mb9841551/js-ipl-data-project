// Find the bowler with the best economy in super overs

const csv = require("csv-parser");      //Imported the csv-parser module which helps in parsing csv files.
const fs = require("fs");              //Imported the fs module which helps in reading and writing into csv files.

const deliveriesData = [];

fs.createReadStream("../data/deliveries.csv")               //fs module reads from the path, then pipes it to csv writable stream and perform event handling
.pipe(csv())
.on("data" ,(data) => deliveriesData.push(data))            //push the incoming data in form of rows to deliveriesData Array
.on("error" , (error) => {
    if(error){
        console.log(error);
    }
})
.on("end" , () => {
    economyInSuperOvers(deliveriesData);              //call the function
})

function economyInSuperOvers(deliveriesData){
    const superOverBowlers = {};         //empty data
    for(let deliveries of deliveriesData){
        if(deliveries.is_super_over === "1"){         //if there is a superover
            if(superOverBowlers.hasOwnProperty(deliveries.bowler) === false){      //to check if superoverbowlers object has deliveries.bowler property or not
                superOverBowlers[deliveries.bowler] = {};
                superOverBowlers[deliveries.bowler] ={    //assign new property of empty object
                    runs : Number(deliveries.total_runs),
                    balls : 1,
                }
            }
            else{
                superOverBowlers[deliveries.bowler].runs += Number(deliveries.total_runs);      //add totalruns to runs property of previous empty object
                superOverBowlers[deliveries.bowler].balls++; //add by 1 to the previous value
            }
        } 
    }
    for(let bowler in superOverBowlers){         
        let runs = superOverBowlers[bowler].runs;
        let balls = superOverBowlers[bowler].balls;
        superOverBowlers[bowler].economy  = runs/Number((balls/6));    //add new property economy with help of old propertied=s runs and balls
        delete superOverBowlers[bowler].runs;   //deleted runs  and balls as it is not needed
        delete superOverBowlers[bowler].balls;
    }
    let mostEconomicalBowler = null;
    let minEconomy = Infinity;
    for (let  bowler in superOverBowlers) {           //to find most ecnomical bowler by using comparison through loops
        let economy = superOverBowlers[bowler].economy;
        if (economy < minEconomy) {
          minEconomy = economy;
          mostEconomicalBowler = bowler;
        }
    }
    let finalObj = {};
    finalObj[mostEconomicalBowler] = minEconomy;   //assigned most Economical bowler to minEcnomy property in an empty object .
    // console.log(finalObj);
    fs.writeFile("../public/output/economySuperOvers.json",JSON.stringify(finalObj),(error) => {    //fs module writes or dump it as JSON file into specified path and error is used as callback function
        if(error){
            console.log(error);
        }
    })
}
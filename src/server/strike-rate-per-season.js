// Find the strike rate of a batsman for each season

const csv = require("csv-parser");          //Imported the csv-parser module which helps in parsing csv files.
const fs = require("fs");                   //Imported the fs module which helps in reading and writing into csv files.

let matchData = [];
let deliveriesData = [];

fs.createReadStream("../data/matches.csv")          //fs module reads from the path, then pipes it to csv writable stream and perform event handling
.pipe(csv())
.on("data" , (data) => matchData.push(data))           //push the incoming data in form of rows to matchData Array
.on("error" , (error) => {
    if(error){
        console.log(error);
    }
})
.on("end" , () => {
    fs.createReadStream("../data/deliveries.csv")
    .pipe(csv())
    .on("data" , (data) => deliveriesData.push(data))                //push the incoming data in form of rows to deliveriesData Array
    .on("error" , (error) => {
        if(error){
            console.log(error);
        }
    })
    .on("end" , () => {
        strikeRatePerSeason(matchData,deliveriesData);        //call the function
    });
});

function strikeRatePerSeason(matchData , deliveriesData){
    const seasonId = {};  //emoty object which will have all the ids of each season

    for(let match of matchData){
        let season = match.season;  
        if(seasonId.hasOwnProperty(season) === false){      //to check if seasonId has season property or not
            seasonId[season] = [];            // if not create an empty array
            seasonId[season].push(match.id);   //push the first id 
        }
        else{
            seasonId[season].push(match.id);  //push the remaining ids if season property is found.
        }
    }
    // console.log(seasonId);
    let batsmanDetails = {};  //empty object
    for(let year in seasonId){
        for(let object of deliveriesData){
            if(seasonId[year].includes(object.match_id)){        //if seasonid of that particular year has that matchid
                if(batsmanDetails.hasOwnProperty(object.batsman) === false){   //if batsmanDetails has batsman(object.batsman) property or not
                    batsmanDetails[object.batsman] = {}; //initialise an empty object
                }
                if(batsmanDetails[object.batsman].hasOwnProperty(year) === false){   //to check if batsman has year property or not
                    batsmanDetails[object.batsman][year] = {};     //created an empty object
                    batsmanDetails[object.batsman][year] = {       //give this object two new properties
                        runs : Number(object.batsman_runs) ,       //runs to be batsman runs  and balls to 1
                        balls : 1 ,
                    }
                }
                else{
                    batsmanDetails[object.batsman][year].runs += Number(object.batsman_runs);     //if batsman has year property then , add it to previous values
                    batsmanDetails[object.batsman][year].balls++;
                }
            }
        }
    }
    // console.log(batsmanDetails);
    const ansObj = batsmanWithStrikeRate(batsmanDetails);   //call the function to get an object which has strike rate in it.
    fs.writeFile("../public/output/strikeRatePerSeason.json",JSON.stringify(ansObj),(error) => {           //fs module writes or dump it as JSON file into specified path and error is used as callback function
        if(error){
            console.log(error);
        }
    });

}

function batsmanWithStrikeRate(batsmanDetails){
    let strikeRate = {};          //empty object
    for(let batsman in batsmanDetails){
        strikeRate[batsman] = {};    //empty object
        for(let year in batsmanDetails[batsman]){
            let runs = batsmanDetails[batsman][year].runs;   
            let balls = batsmanDetails[batsman][year].balls;
            strikeRate[batsman][year] = {SR : (runs/balls)*100};   //get the strike rate by calculation
        }
    }
    // console.log(strikeRate);
    return strikeRate;
}



